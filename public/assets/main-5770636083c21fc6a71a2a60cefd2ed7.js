(function() {
  var rgtask;

  rgtask = angular.module('rgtask', ['ngRoute', 'ngAnimate']).config([
    "$httpProvider", function($httpProvider) {
      var interceptor;
      $httpProvider.defaults.headers.common["X-CSRF-Token"] = $("meta[name=csrf-token]").attr("content");
      interceptor = [
        "$location", "$rootScope", "$q", function($location, $rootScope, $q) {
          var error, success;
          success = function(response) {
            return response;
          };
          error = function(response) {
            if (response.status === 401) {
              $rootScope.$broadcast("event:unauthorized");
              $location.path("/login");
              return response;
            }
            return $q.reject(response);
          };
          return function(promise) {
            return promise.then(success, error);
          };
        }
      ];
      return $httpProvider.responseInterceptors.push(interceptor);
    }
  ]).config([
    '$routeProvider', function($routeProvider) {
      $routeProvider.when('/register', {
        templateUrl: '../assets/templates/register.html',
        controller: 'SessionsCtrl'
      });
      $routeProvider.when('/login', {
        templateUrl: '../assets/templates/login.html',
        controller: 'SessionsCtrl'
      });
      return $routeProvider.otherwise({
        templateUrl: '../assets/templates/index.html',
        controller: 'MainCtrl'
      });
    }
  ]);

}).call(this);
