class Task < ActiveRecord::Base
	belongs_to :project
	default_scope { order(project_id: :desc, priority: :desc) }
	validates :name, presence: { message: 'Please provide a name' }
	validates_presence_of :project
	attr_accessible :name, :priority, :project_id
end