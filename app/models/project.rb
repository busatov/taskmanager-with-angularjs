class Project < ActiveRecord::Base
	has_many :tasks, dependent: :destroy
	validates :name, presence: { message: 'Please provide a name' }
	attr_accessible :name
end