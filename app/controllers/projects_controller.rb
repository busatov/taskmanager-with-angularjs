class ProjectsController < ApplicationController
	def create
		project = Project.new
		project.name = params[:name]
		user = current_user
		project.user_id = user.id

		if project.valid?
			project.save
		else
			render "public/422", status: 422
			return
		end

		respond_to do |format|
      		format.json { render :json => project.as_json(include:[:tasks]) }
    	end

	end

	def destroy
		Project.find(params[:id]).destroy
		respond_to do |format|
    		format.json { render :json => {:message => "Destroyed"} }
  		end
  end

  def update
    Project.find_by(id: params[:id]).update_attribute(:name, params[:name])
    respond_to do |format|
      format.json { render :json => {:message => "Success"} }
    end
  end

	protected
    def authenticate
      if !user_signed_in?
        render :status => 401,
           :json => { :success => false,
                      :info => "Login Credentials Failed"
           }
      end
    end
end