class UsersController < ApplicationController
  before_filter :authenticate
  # GET /users
  def index
    user = current_user
    projects = Project.where user_id: user.id
    ids=projects.map(&:id)
    tasks = Task.where(project_id: ids)
    respond_to do |format|
      format.json { render json: {user: user, projects: projects.as_json(include:[:tasks])} }
    end
  end

  protected
    def authenticate
      if !user_signed_in?
        render :status => 401,
           :json => { :success => false,
                      :info => "Login Credentials Failed"
           }
      end
    end

end
