class TasksController < ApplicationController
	def create
		task = Task.new
		task.name = params[:name]
		task.project_id = params[:project_id]
		if Project.find(params[:project_id]).tasks.first == nil
			task.priority = 1
		else
			task.priority = Project.find(params[:project_id]).tasks.maximum(:priority) + 1
		end

		if task.valid?
			task.save

		else
			render "public/422", status: 422
			return
		end

		respond_to do |format|
      		format.json { render :json => task.as_json }
    	end

	end

	def update
		if params[:act] == "increment"
			task_current = Task.find_by(id: params[:id])
  		project_current = Project.find_by(id: task_current.project_id)
  		max = project_current.tasks.maximum("priority")
  		if task_current.priority != max
        tasks_of_current_project = Task.where(project_id: task_current.project_id)
	      task_next = tasks_of_current_project[tasks_of_current_project.index(task_current) - 1]
	      task_current_priority = task_current.priority
        task_next_priority = task_next.priority
	      task_next.priority = task_current_priority
        task_current.priority = task_next_priority
	      task_current.save
	      task_next.save
	      respond_to do |format|
    			format.json { render :json => {:message => "Success"} }
  			end
  		else
  			respond_to do |format|
    			format.json { render :json => {:message => "Fail"} }
  			end
  		end
  	end
  	if params[:act] == 'decrement'
  		task_current = Task.find_by(id: params[:id])
	    if task_current.priority != 1
	      tasks_of_current_project = Task.where(project_id: task_current.project_id)
        task_next = tasks_of_current_project[tasks_of_current_project.index(task_current) + 1]
        task_current_priority = task_current.priority
        task_next_priority = task_next.priority
	      task_current.priority = task_next_priority
	      task_next.priority = task_current_priority
	      task_current.save
	      task_next.save
	      respond_to do |format|
    			format.json { render :json => {:message => "Success"} }
  			end
  		else
  			respond_to do |format|
    			format.json { render :json => {:message => "Fail"} }
  			end
    	end
  	end
  	if params[:act] == 'edit'
  		Task.find_by(id: params[:id]).update_attribute(:name, params[:name])
  		respond_to do |format|
    			format.json { render :json => {:message => "Success"} }
  			end
  	end
  	if params[:act] == 'done'
  		Task.find_by(id: params[:id]).update_attribute(:done, params[:done])
  		respond_to do |format|
    			format.json { render :json => {:message => "Success"} }
  			end
  	end

	end

	def destroy
		Task.find(params[:id]).destroy
		respond_to do |format|
    		format.json { render :json => {:message => "Destroyed"} }
  	end
  end
end