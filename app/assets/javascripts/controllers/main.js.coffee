@MainCtrl = ($scope, $http, userData) ->
  $scope.user = userData.data
  $scope.create_project =
    project_name: null

  $scope.edit_task_modal =
    task: []
    project_index: null
    edited_task_name: null

  $scope.edit_project_modal =
    project: []
    project_index: null
    edited_project_name: null

  $scope.task_delete = (task, project_index) ->
    index = userData.data.projects[project_index].tasks.indexOf(task)
    userData.data.projects[project_index].tasks.splice(index, 1)
    $http(
      method: "DELETE"
      url: './tasks/' + task.id
      ).success( ->
        console.log('Succesfully delete task')
        ).error( ->
        console.log('Failed to delete task')
      )

  $scope.task_done = (task) ->
    done = task.done
    $http(
      method: "PATCH"
      url: './tasks/' + task.id + '?act=done'
      data:
        done: !done
      ).success( ->
        console.log('Succesfully mark as done')
        ).error( ->
          console.log('Failed to increment')
      )

  $scope.priority_up = (task, project_index) ->
    index = userData.data.projects[project_index].tasks.indexOf(task)
    if (index <= 0 || index >= userData.data.projects[project_index].tasks.length)
      return;
    temp = userData.data.projects[project_index].tasks[index];
    userData.data.projects[project_index].tasks[index] = userData.data.projects[project_index].tasks[index - 1];
    userData.data.projects[project_index].tasks[index - 1] = temp;
    $http(
      method: "PATCH"
      url: './tasks/' + task.id + '?act=increment'
      ).success( ->
        console.log('Succesfully increment priority')
        ).error( ->
          console.log('Failed to increment')
      )

  $scope.priority_down = (task, project_index) ->
    index = userData.data.projects[project_index].tasks.indexOf(task)
    if (index < 0 || index >= (userData.data.projects[project_index].tasks.length-1))
      return;
    temp = userData.data.projects[project_index].tasks[index];
    userData.data.projects[project_index].tasks[index] = userData.data.projects[project_index].tasks[index + 1];
    userData.data.projects[project_index].tasks[index + 1] = temp;
    $http(
      method: "PATCH"
      url: './tasks/' + task.id + '?act=decrement'
      ).success( ->
        console.log('Succesfully decrement priority')
        ).error( ->
          console.log('Failed to decrement')
      )

  $scope.delete_project = (index) ->
    $http(
      method: "DELETE"
      url: './projects/' + userData.data.projects[index].id
      ).success( ->
        userData.data.projects.splice(index, 1)
        console.log('Succesfully delete project')
        ).error( ->
        console.log('Failed to delete project')
      )

  $scope.create_task = (index, task_name) ->
    $http(
      method: "POST"
      url: "./tasks.json"
      data:
        name: task_name
        project_id: userData.data.projects[index].id
        ).success( (data) ->
        userData.data.projects[index].create_task_name = ''
        userData.data.projects[index].tasks.splice(0, 0, data)
        console.log('Succesfully created task')
        ).error( ->
        console.error('Failed to create new task.')
      )

  $scope.edit_task_modal = (project_index, task) ->
    $scope.edit_task_modal.task = task
    $scope.edit_task_modal.project_index = project_index

  $scope.edit_project_modal = (project_index, project) ->
    $scope.edit_project_modal.project = project
    $scope.edit_project_modal.project_index = project_index

  $scope.edit_task_modal_clear = ->
    $scope.edit_task_modal.task = null
    $scope.edit_task_modal.project_index = ''
    $scope.edit_task_modal.edited_task_name = ''

  $scope.edit_project_modal_clear = ->
    $scope.edit_project_modal.project = null
    $scope.edit_project_modal.project_index = ''
    $scope.edit_project_modal.edited_project_name = ''

  $scope.edit_task = ->
    task_index = userData.data.projects[$scope.edit_task_modal.project_index].tasks.indexOf($scope.edit_task_modal.task)
    userData.data.projects[$scope.edit_task_modal.project_index].tasks[task_index].name = $scope.edit_task_modal.edited_task_name
    $http(
      method: "PATCH"
      url: "./tasks/" + $scope.edit_task_modal.task.id + '?act=edit'
      data:
        name: $scope.edit_task_modal.edited_task_name).success( ->
        $scope.edit_task_modal_clear()
        ).error( ->
        console.error('Failed')
      )

  $scope.edit_project = ->
    project_index = userData.data.projects.indexOf($scope.edit_project_modal.project)
    userData.data.projects[$scope.edit_project_modal.project_index].name = $scope.edit_project_modal.edited_project_name
    $http(
      method: "PATCH"
      url: "./projects/" + $scope.edit_project_modal.project.id
      data:
        name: $scope.edit_project_modal.edited_project_name).success( ->
      $scope.edit_project_modal_clear()
    ).error( ->
      console.error('Failed')
    )

  $scope.create_project = ->
    $http(
      method: "POST"
      url: "./projects.json"
      data:
        name: $scope.create_project.project_name).success( (data) ->
        userData.data.projects.push(data)
        $scope.create_project.project_name = ''
        console.log('Succesfully created project')
        ).error( ->
        console.error('Failed to create new project.')
      )

  userData.loadData()