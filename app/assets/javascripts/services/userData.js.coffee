angular.module('rgtask').factory('userData', ['$http', ($http) ->

  userData =
    data:
      user:'loading' 
      projects:[]
      tasks:[]

  userData.loadData = ->
    $http.get('./users.json').success( (data) ->
      userData.data.user = data.user
      
      for element in data.projects
        element["create_task_name"] = ''
        userData.data.projects.push(element)
      console.log('Successfully loaded posts.')
    ).error( ->
      console.error('Failed to load posts.')
    )

  userData.cleanData = ->
    userData.data.user = null
    userData.data.projects = []
    userData.data.tasks = []

  return userData

])
