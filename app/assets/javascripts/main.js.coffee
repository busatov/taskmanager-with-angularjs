# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

rgtask = angular.module('rgtask', ['ngRoute', 'ngAnimate'])

.config(["$httpProvider"
  ($httpProvider) ->
    $httpProvider.defaults.headers.common["X-CSRF-Token"] = $("meta[name=csrf-token]").attr("content")
    interceptor = [
      "$location"
      "$rootScope"
      "$q"
      ($location, $rootScope, $q) ->
        success = (response) ->
          response
        error = (response) ->
          if response.status is 401
            $rootScope.$broadcast "event:unauthorized"
            $location.path "/login"
            return response
          $q.reject response
        return (promise) ->
          promise.then success, error
    ]
    $httpProvider.responseInterceptors.push interceptor
  ])

.config(['$routeProvider', ($routeProvider) ->
  $routeProvider.when('/register', { templateUrl: '../assets/templates/register.html', controller: 'SessionsCtrl' } )
  $routeProvider.when('/login', { templateUrl: '../assets/templates/login.html', controller: 'SessionsCtrl' } )

  $routeProvider.otherwise({ templateUrl: '../assets/templates/index.html', controller: 'MainCtrl'} )

])
