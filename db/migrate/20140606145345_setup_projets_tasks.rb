class SetupProjetsTasks < ActiveRecord::Migration
  def change
  	create_table :projects do |t|
  		t.string :name
  		t.references :user, index: true
  	end

  	create_table :tasks do |t|
  		t.string :name
      t.integer :priority, unsigned: true
  		t.references :project, index: true
  	end
  end
end
